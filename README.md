# sofdb

Recreating a **database** system, including:
- a databse file format.
- a database cli tool.[^1]
- a database server.[^1]

[^1]: Not yet implemented.

Why:
- To learn how to make a database system.
- To make integrate well with my other projects & requirement.

## Usage

You'll have to `stack run` after modifying `/app/Main.hs/` on your own.

## `.sof.db` (Database File) Syntax

---
**Syntax** is:
- `TABLE <table-name>` line.
- `CHECKSUM <checksum>` line.
- `ID <col-1-name> <col-2-name> ...` columns line.
- `<id> <col-1-value> <col-2-value> ...` for every line after.

(everything aside from `<checksum>` & `<id>` has to a string enclosed in quotes(`"`), and no quotes(`"`) may occur inside.)\
(single quotes `'` are fine.)

---
Comments are:
- Anything before the `TABLE <table-name>` line.

## Notes
- Will **not work properly** if:
    - number of columns is inconsistent. 
    - there are any empty lines after the `TABLE` line.
    - a `"` character is used in a `<col-#-value>`.
        - not even `\"` is supported.
    - the `CHECKSUM` line doesn't immediatly follow the `TABLE` line.
    - the column names line doesn't immediatly follow the `CHECKSUM` line.
    - the `ID` column is not the first line.
    - the `ID` value is missing in a row, or is enlosed in quotes.

## Licence

```
Copyright (C) 2023 sof

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```


