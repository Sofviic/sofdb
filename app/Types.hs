module Types where

import Data.Text as T

type Table = (Text,[(Int, Col -> Maybe Text)])
type TabulatedTable = (Text,[(Int, Col, Text)])
type Col = Text
type Row = [Text]
