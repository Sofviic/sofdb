module UtilsPrelude where

concatWith :: [a] -> [[a]] -> [a]
concatWith _ []     = []
concatWith _ [s]    = s
concatWith w (s:ss) = s ++ w ++ concatWith w ss

prefixes :: Eq a => [a] -> [a] -> Bool
prefixes [] _          = True
prefixes _  []         = False
prefixes (a:as) (b:bs) = if a /= b
                                then False
                                else prefixes as bs

dropLast :: Int -> [a] -> [a]
dropLast n = reverse . drop n . reverse

quote :: String -> String
quote = delimit '"' '"'

quotes :: String -> [String]
quotes = delimited '"' '"'

delimit :: Char -> Char -> String -> String
delimit ld rd = (pure ld ++) . (++ pure rd)

delimited :: Char -> Char -> String -> [String]
delimited _  _  [] = []
delimited ld rd s  = [takeWhile (/= rd) . drop 1 . dropWhile (/= ld) $ s] 
                        ++ delimited ld rd (drop 1 . dropWhile (/= rd) . drop 1 . dropWhile (/= ld) $ s)

extractStartNum :: String -> Maybe (Int,String)
extractStartNum s = factorMaybe (headS (words s) >>= readMaybe, (Just . unwords . tail . words) s)

factorMaybe :: (Maybe a, Maybe b) -> Maybe (a,b)
factorMaybe (Nothing,_      ) = Nothing
factorMaybe (_      ,Nothing) = Nothing
factorMaybe (Just a , Just b) = Just (a,b)

headS :: [a] -> Maybe a
headS []    = Nothing
headS (x:_) = Just x

fromMaybe :: a -> Maybe a -> a
fromMaybe e Nothing  = e
fromMaybe _ (Just e) = e

catMaybes :: [Maybe a] -> [a] 
catMaybes []            = []
catMaybes (Just x:xs)   = x : catMaybes xs
catMaybes (Nothing:xs)  = catMaybes xs

-- Note: probs inefficent
readMaybe :: Read a => String -> Maybe a
readMaybe s = case reads s of
                [] -> Nothing
                ls -> case filter (null.snd) ls of
                        [] -> Nothing
                        as -> fmap fst . headS $ as

(<||>) :: (a -> Maybe b) -> (a -> Maybe b) -> a -> Maybe b
(<||>) f g a = f a <|> g a

(<|>) :: Maybe a -> Maybe a -> Maybe a
Nothing <|> Nothing = Nothing
Nothing <|> b       = b
a       <|> _       = a

infixr 8 .#.
(.#.) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
f .#. g = (f .) . g
infixr 8 .##.
(.##.) :: (c -> d) -> (a -> b1 -> b2 -> c) -> a -> b1 -> b2 -> d
f .##. g = ((f .) .) . g
infixr 8 .###.
(.###.) :: (c -> d) -> (a -> b1 -> b2 -> b3 -> c) -> a -> b1 -> b2 -> b3 -> d
f .###. g = (((f .) .) .) . g
