{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module CLI.Main where

import Prelude as P hiding(putStrLn,readFile,unwords,unlines)
import DB.Integrity
import DB.Utils
import DB.Parser
import DB.Operations
import DB.Generator
import Data.Text as T
import Data.Text.IO.Utf8 as T
import System.Environment

execCommand :: IO ()
execCommand = do
                args <- getArgs
                case args of
                    ("init":filename:cols) -> cmdInit filename cols
                    ("-i"  :filename:cols) -> cmdInit filename cols
                    ("select":filename:cols) -> cmdSelect filename cols
                    ("-s"    :filename:cols) -> cmdSelect filename cols
                    ("write":filename:vals) -> cmdWrite filename vals
                    ("-w"   :filename:vals) -> cmdWrite filename vals
                    ("recompute":filename:_) -> cmdRecompute filename
                    ("-r"       :filename:_) -> cmdRecompute filename
                    ("help":_) -> cmdHelp
                    ("-h"  :_) -> cmdHelp
                    _ -> cmdHelp

cmdInit :: FilePath -> [String] -> IO ()
cmdInit fp cols = genTableFile (fp <> ".sof.db") (genEmptyTable $ T.pack fp) (fmap T.pack cols)


cmdSelect :: FilePath -> [String] -> IO ()
cmdSelect fp cols = do
                        db <- readFile fp
                        let (_checksum,_colnames,table) = (parseDB . T.lines) db
                        putStrLn . untabs . ("ID":) $ fmap T.pack cols
                        putStrLn "--------------------------------------------------------------------------------"
                        putStrLn . unlines . fmap untabs . select (fmap T.pack cols) $ table
                        where untabs = intercalate (singleton '\t')

cmdWrite :: FilePath -> [String] -> IO ()
cmdWrite fp vals = do
                    db <- readFile fp
                    let (_oldChecksum,colnames,oldtable) = (parseDB . T.lines) db
                    let newtable = record (mappify colnames $ fmap T.pack vals) $ oldtable
                    genTableFile fp newtable colnames

cmdRecompute :: FilePath -> IO ()
cmdRecompute fp = do
                    db <- readFile fp
                    let (oldChecksum,colnames,table) = (parseDB . T.lines) db
                    let newChecksum = checksumDB table colnames
                    putStrLn $ "old CHECKSUM = " <> oldChecksum
                    putStrLn $ "new CHECKSUM = " <> newChecksum
                    genTableFile fp table colnames
                    putStrLn "refreshed."


cmdHelp :: IO ()
cmdHelp = do 
            putStrLn "Usage: sofdb -i <filename> <cols>..."
            putStrLn "     | sofdb -s <filename> <cols>..."
            putStrLn "     | sofdb -w <filename> <vals>..."
            putStrLn "     | sofdb -r <filename> ..."
            putStrLn "     | sofdb -h ..."
            putStrLn "------------------------------------------"
            putStrLn "# sofdb -i <filename> <cols>..."
            putStrLn "creates a new empty .sof.db table file called <filename>, with <cols> columns."
            putStrLn "------------------------------------------"
            putStrLn "# sofdb -s <filename> <cols>..."
            putStrLn "shows contents of table in <filename>, but only the <cols> columns."
            putStrLn "------------------------------------------"
            putStrLn "# sofdb -w <filename> <vals>..."
            putStrLn "appends a row to the table in <filename>, with the values <vals> left to right."
            putStrLn "(Note: extras will be blank)"
            putStrLn "------------------------------------------"
            putStrLn "# sofdb -r <filename> ..."
            putStrLn "recomputes the checksum for the table in <filename>."
            putStrLn "------------------------------------------"
            putStrLn "# sofdb -h ..."
            putStrLn "shows this message with list of args below."
            putStrLn ""
            putStrLn "Args were:"
            displayArgs
            putStrLn ""

displayArgs :: IO ()
displayArgs = getArgs >>= displayNumbered 1 . fmap T.pack

displayNumbered :: Int -> [Text] -> IO ()
displayNumbered n = fmap (const ())
                    . sequence
                    . fmap (putStrLn . \(i,e) -> (T.pack.show) i <> ": " <> e)
                    . P.zip [n..]

    