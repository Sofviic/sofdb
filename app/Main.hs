{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import Prelude as P
import Types
import DB.Integrity
import DB.Utils
import DB.Parser
import DB.Operations
import DB.Generator

import Data.Text as T
import Data.Text.IO.Utf8 as T


-- TODO : verify that syntax is correct

-- TODO : add table commands / cli
    -- TODO : add `table filter`
    -- TODO : add `table map`
    -- TODO : add `table mapon`
    -- TODO : add `table col-add`
    -- TODO : add `table col-rename`
    -- TODO : add `table col-remove`
    -- TODO : add `table delete`
    -- TODO : add `table copy`
    -- TODO : add `table rename`

import qualified CLI.Main

main :: IO ()
main = CLI.Main.execCommand



test2 :: IO ()
test2 = do
        let colnames = ["n", "n^2", "T(n) := \\frac{n(n+1)}{2}"]
        let table = 
                      record (mappify colnames $ fmap (T.pack.show) [6, 6^2, 6*(6+1)/2])
                    . record (mappify colnames $ fmap (T.pack.show) [5, 5^2, 5*(5+1)/2])
                    . record (mappify colnames $ fmap (T.pack.show) [4, 4^2, 4*(4+1)/2])
                    . record (mappify colnames $ fmap (T.pack.show) [3, 3^2, 3*(3+1)/2])
                    . record (mappify colnames $ fmap (T.pack.show) [2, 2^2, 2*(2+1)/2])
                    . record (mappify colnames $ fmap (T.pack.show) [1, 1^2, 1*(1+1)/2])
                    $ genEmptyTable "Equations : Triangular Numbers"
        genTableFile "src/test.sof.db" table $ colnames
        T.putStrLn . genTableFileContent table $ colnames
        return ()


test1 :: IO ()
test1 = do
            db <- T.readFile "src/sampleDB2.sof.db"
            let (checksum,colnames,table) = parseDB . T.lines $ db
            let computeChecksum = checksumDB table colnames
            T.putStrLn $ "recieved CHECKSUM = " <> checksum
            T.putStrLn $ "computed CHECKSUM = " <> computeChecksum
            --T.putStrLn . showRows . select ["Name", "Language"] $ table
            genTableFile "src/test.sof.db" table $ colnames
            T.putStrLn . genTableFileContent table $ colnames
            return ()





