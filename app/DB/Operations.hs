{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module DB.Operations where

import Prelude as P
import Types
import Utils
import DB.Utils
import Data.Text as T

select :: [Col] -> Table -> [Row]
select colnames (_,table) = fmap (\(pk,val) -> (pack.show) pk : fmap (fromMaybe "" . val) colnames) table

record :: (Col -> Maybe Text) -> Table -> Table
record r (name,table) = (name, table ++ [(rid,r)])
                        where
                            rid = firstElemNotIn (-1) [0..] $ fmap fst table

delete :: Int -> Table -> Table
delete rid (name,table) = (name, P.filter ((/= rid) . fst) table)

