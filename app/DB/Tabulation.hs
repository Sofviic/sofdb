{-# LANGUAGE NoImplicitPrelude #-}
module DB.Tabulation where

import Prelude hiding(zip)
import Types
import Utils
import Data.List as L
import Data.Text as T

tabulateTable :: Table -> [Col] -> TabulatedTable
tabulateTable (name,con) cols = (name,matrix)
                                    where matrix = [(rid,col,val) | (rid,row) <- con
                                                                  , col <- cols
                                                                  , let mval = row col
                                                                  , mval /= Nothing
                                                                  , let Just val = mval]

untabulateTable :: TabulatedTable -> ([Col],Table)
untabulateTable (_name,_matrix) = error "`untabulateTable` is not yet implemented."
