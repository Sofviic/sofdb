{-# LANGUAGE OverloadedStrings #-}
module DB.Generator where

import Utils
import Types
import DB.Integrity
import DB.Operations
import Data.Text as T
import Data.Text.IO.Utf8 as T

genTableFile :: FilePath -> Table -> [Col] -> IO ()
genTableFile fp = T.writeFile fp .#. genTableFileContent

genTableFileContent :: Table -> [Col] -> Text
genTableFileContent t@(name,_) cols = T.unlines 
                                        . (("TABLE " <> quote name):) 
                                        . (("CHECKSUM " <> checksumDB t cols):) 
                                        . fmap (T.intercalate "\t") 
                                        . (("ID" : fmap quote cols):)
                                        . (fmap.mapNonzeroth) quote
                                        . select cols
                                        $ t

genTableFileForcedContent :: Table -> [Col] -> Text
genTableFileForcedContent = undefined -- TODO ; uses all cols

genEmptyTable :: Text -> Table
genEmptyTable name = (name,[])

showRows :: [Row] -> Text
showRows = T.unlines . fmap (pack . show)
