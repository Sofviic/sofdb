{-# LANGUAGE OverloadedStrings #-}
module DB.Parser where

import Utils
import Types
import DB.Utils
import Data.List as L
import Data.Text as T

parseDB :: [Text] -> (Text,[Col],Table)
parseDB s = (,,) checksum cols $ (,) (tablename) $ [(key, mappify (quotes columns) (quotes rval)) 
                                                      | row <- content
                                                      , (key,rval) <- (catEithers . pure . extractStartNum) row]
            where (tablenamerow:checksumrow:columns:content) = L.dropWhile (not . T.isPrefixOf "TABLE") s
                  (tablename :_)           = quotes tablenamerow
                  ("CHECKSUM":checksum:_)  = T.words checksumrow
                  cols = quotes columns

