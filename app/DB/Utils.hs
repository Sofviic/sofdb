module DB.Utils where

import Utils
import Types
import Data.List as L
import Data.Text as T

mappify :: [Col] -> Row -> (Col -> Maybe Text)
mappify colnames vals colname = headS . fmap snd . L.filter (\(k,_) -> k == colname) $ L.zip colnames vals
