module DB.Integrity where

import Types
import DB.Tabulation
import Data.Text as T
import qualified Data.ByteString.Char8 as B
import Crypto.Hash

verifyChecksum :: Text -> Table -> [Col] -> Bool
verifyChecksum checksum t cols = checksum == checksumDB t cols

checksumDB :: Table -> [Col] -> Text
checksumDB t cols = (T.pack . show . hashWith SHA256 . B.pack . show) $ tabulateTable t cols

