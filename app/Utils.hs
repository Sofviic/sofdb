{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Utils where
        
import Prelude as P hiding (words, unwords, lines, unlines)
import Data.Text as T
import Data.Text.Read as T

concatWith :: [a] -> [[a]] -> [a]
concatWith _ []     = []
concatWith _ [s]    = s
concatWith w (s:ss) = s ++ w ++ concatWith w ss

quote :: Text -> Text
quote = delimit '"' '"'

quotes :: Text -> [Text]
quotes = delimited '"' '"'

delimit :: Char -> Char -> Text -> Text
delimit ld rd = (singleton ld <>) . (<> singleton rd)

delimited :: Char -> Char -> Text -> [Text]
delimited _  _  "" = []
delimited ld rd s  = [T.takeWhile (/= rd) . T.drop 1 . T.dropWhile (/= ld) $ s] 
                        ++ delimited ld rd (T.drop 1 . T.dropWhile (/= rd) . T.drop 1 . T.dropWhile (/= ld) $ s)

firstElemNotIn :: Eq a => a -> [a] -> [a] -> a
firstElemNotIn e []     _ = e
firstElemNotIn e (a:as) b = if not (a `P.elem` b)
                                then a
                                else firstElemNotIn e as b

type Error = String

extractStartNum :: Text -> Either Error (Int,Text)
extractStartNum s = factorEither (fmap fst $ headE (words s) >>= decimal, (Right . unwords . P.tail . words) s)

factorEither :: Semigroup c => (Either c a, Either c b) -> Either c (a,b)
factorEither (Left c1, Left c2) = Left $ c1 <> c2
factorEither (Left c , _      ) = Left c
factorEither (_      , Left c ) = Left c
factorEither (Right a, Right b) = Right (a,b)

headS :: [a] -> Maybe a
headS []    = Nothing
headS (x:_) = Just x

headE :: [a] -> Either Error a
headE []    = Left "Used headE with empty list."
headE (x:_) = Right x

mapStartingFrom :: Int -> (a -> a) -> [a] -> [a]
mapStartingFrom n f x = P.take n x ++ fmap f (P.drop n x)

mapNonzeroth :: (a -> a) -> [a] -> [a]
mapNonzeroth = mapStartingFrom 1

fromMaybe :: a -> Maybe a -> a
fromMaybe e Nothing  = e
fromMaybe _ (Just e) = e

catMaybes :: [Maybe a] -> [a] 
catMaybes []            = []
catMaybes (Just x:xs)   = x : catMaybes xs
catMaybes (Nothing:xs)  = catMaybes xs

catEithers :: [Either c a] -> [a] 
catEithers []            = []
catEithers (Right x:xs)   = x : catEithers xs
catEithers (Left  _:xs)  = catEithers xs

(<||>) :: (a -> Maybe b) -> (a -> Maybe b) -> a -> Maybe b
(<||>) f g a = f a <|> g a

(<|>) :: Maybe a -> Maybe a -> Maybe a
Nothing <|> Nothing = Nothing
Nothing <|> b       = b
a       <|> _       = a

infixr 8 .#.
(.#.) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
f .#. g = (f .) . g
infixr 8 .##.
(.##.) :: (c -> d) -> (a -> b1 -> b2 -> c) -> a -> b1 -> b2 -> d
f .##. g = ((f .) .) . g
infixr 8 .###.
(.###.) :: (c -> d) -> (a -> b1 -> b2 -> b3 -> c) -> a -> b1 -> b2 -> b3 -> d
f .###. g = (((f .) .) .) . g
